"""
Python içerisinde hata ayıklama try/except bloğu ile yapılır.
'try' altında çalıştırmak istediğimiz ama olası bir hata verme potansiyeli olan kodumuz yazılır.
Eğer bu kod hata verirse 'except' bloğu içerisine geçer.
Bir try/except içerisinde sadece tek bir 'try' olurken birden fazla 'except' yer alabilir.
except kullanılırken yanında hata tipini de belirtmemiz gereklidir.
Belirtmezsek kod gene çalışır ancak bu sefer python hataları dışındaki olayları da kapsar.
Bu da işletim sistemi tarafında beklenmedik problemlere sebep açabilir.
Bütün Python hata tiplerini kapsamak için 'Exception' tipini kullanırız.
Hata mesajımızın detaylarını almak için hata tipinin ardından 'as e' olarak yazarız.
Bu sayede except bloğu içerisinde hatamız 'e' değişkeni ile erişilebilir olur.

Bir try içerisinde birden fazla hata olabileceği durumda ilk hangi hata denk gelirse
sadece o hatanın işlemler gerçekleşir.
Çünkü herhangi bir hata mesajı alındığında 'try' bloğu sonlanır.
"""

ornek_dict = {'name': 'Mahmut', 'surname': 'Tuncer'}

try:
    10 / 0
    print(ornek_dict['age'])
except ValueError as e:
    print('ValueError aldik be abi', e)
except KeyError as e:
    print(f"{e} degeri bu dict icerisinde yoktur!!!!")
except Exception as e:
    print(e)
    print(e.args)
    print(e.__class__)


# Burasi neden except'in tek başına kullanılmaması gerektiği için bir örnektir
# Aklı olan böyle bir şey yazmaz
# Windows kullaniyorsaniz bunu çalıştırmayın.
# Linux kullanıyorsanız da öldürmek için "kill -9" kullanın


while True:
    import time
    try:
        print('Simdi yandiniz')
        time.sleep(2)
    except:  # NOQA
        print('DAHA DA YANDINIZ!!!')
