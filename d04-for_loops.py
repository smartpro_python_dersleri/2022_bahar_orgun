ogrenciler = ['Deniz', 'Kemal', 'Suheyb', 'Cemre']

for ogrenci in ogrenciler:
    print(ogrenci)


# for i in range(1, 51):
#     print(i)


# print(list(range(1, 51)))

hayvanlar = [
    ('Kedi', 'Suzan'),
    ('Kedi', 'Carsi'),
    ('Kedi', 'Pacchi')
]

for tur, ad in hayvanlar:
    print(tur, ad)


ornek_dict = {
    'name': 'Ismail',
    'surname': 'Turut',
}

for key, value in ornek_dict.items():
    print(key, value)


print(list(enumerate(ogrenciler)))

for index, ogrenci in enumerate(ogrenciler):
    print(index, ogrenci)
