ornek_degisken = 'Bu bir ornek degiskendir'

# str: Düz yazı. " ya da ' ile belirtilir.
ornek_str = 'Ben bir ceviz ağacıyım gülhane parkında'
yeni_satir_str = 'Ne sen bunun farkındasın,\nNe de polis farkında.'

tirnak_1_str = 'Sonra ona dedim ki "Ne vereyim abime"'
tirnak_2_str = 'Sanırım bilgisayarımı Göktuğ\'da unuttum. \\'

# print("Ali, Veli, " + "Hüseyin")

# int: Düz sayı. Herhangi bir şey içine yazmanıza gerek yoktur.

ornek_int = 42
print(ornek_int)
# Type komutu kendisine verdigimiz degiskenin tipini belirtir
# print(type(ornek_int))

ornek_int = ornek_int + 2
print(ornek_int)

ornek_int += 2
print(ornek_int)
