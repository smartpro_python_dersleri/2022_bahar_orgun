# Esittir. == ile tanimlanir. Iki degerin de ayni olmasi gerekir
print('\n==')

print('Ali' == 'Ali')  # True
print('ali' == 'Ali')  # False
print(type('Mustafa' == 88))  # Bool -- False
print(('ahmet', 'mehmet') == ('ahmet', 'mehmet'))  # True
print(('ahmet', 'mehmet') == ('mehmet', 'ahmet'))  # False
print({'ahmet', 'mehmet'} == {'mehmet', 'ahmet'})  # True
print({'ahmet', 'mehmet', 'veli'} == {'mehmet', 'ahmet'})  # False
print({'name': 'ismail', 'surname': 'turut'} == {'surname': 'turut', 'name': 'ismail'})  # True
# print(True == 0)  # False
# print(True == 1)  # True
# print(False == 0)  # True


# Esit Degildir. != ile tanimlanir, iki degerin de farkli olmasi gerekir
print('\n!=')

print('Ali' != 'Ali')  # False
print('Mahmut' != 'Kestane')  # True


# Buyuktur. > ile tanimlanir, ilk deger, ikinci degerden buyuk olmasi gereklidir
print('\n>')
print('c' > 'b')  # True
print(12 > 5)  # True
print(42 > 58)  # False
# print(2 > 'a')  # Error, bu iki tip arasi kiyaslama yapamaz
print(True > False)  # True
print(False > True)  # False


# Kucuktur. < ile tanimlanir, ilk sayinin ikinciden kucuk olmasi gerekir
print('\n<')
print(12 < 10)  # False
print(12 < 42)  # True
print(42 < 42)  # False


# Buyuk Esittir. >= ile tanimlanir. Buyuk ya da esit oldugu kosulta True doner
print('\n>=')
print(12 >= 12)  # True
print(12 >= 11)  # True
print(12 >= 13)  # False


# Kucuk Esittir. <= ile tanimlanir. Kucuk ya da esit oldugu kosulda True doner
print('\n<=')
print(12 <= 12)  # True
print(12 <= 11)  # False
print(12 <= 13)  # True


# in. in ile tanimlanir. bir ogenin bir veri setinde olup olmadigini kontrol eder.
print('\nin')
print('ahmet' in ['ahmet', 'mehmet', 'veli'])  # True
print('goktug' in ['ahmet', 'mehmet', 'veli'])  # False
print('name' in {'name': 'Mahmut', 'surname': 'Tuncer'})  # True
print('age' in {'name': 'Mahmut', 'surname': 'Tuncer'})  # False
print('Mahmut' in {'name': 'Mahmut', 'surname': 'Tuncer'})  # False
print('Mahmut' in {'name': 'Mahmut', 'surname': 'Tuncer'}.values())  # True
print('name' in {'name': 'Mahmut', 'surname': 'Tuncer'}.values())  # False


# veya, or ile tanimlanir. iki degerden birinin True olmasi gerekir
print('\nor')
print(True or False)  # True
print(True or True)  # True
print(False or True)  # True
print(False or False)  # False


# Ve, and ile tanimlanir. Iki degerin de True olmasi gerekir
print('\nand')
print(True and False)  # False
print(True and True)  # True
print(False and True)  # False
print(False and False)  # False


# Is....  Birebir ayni oge olmasi lazim
print('\nis')
print('Ali' is 'Ali')  # True
print('ali' is 'Ali')  # False
print(type('Mustafa' is 88))  # Bool -- False
print(('ahmet', 'mehmet') is ('ahmet', 'mehmet'))  # True
print(('ahmet', 'mehmet') is ('mehmet', 'ahmet'))  # False
print({'ahmet', 'mehmet'} is {'mehmet', 'ahmet'})  # False
print({'ahmet', 'mehmet', 'veli'} is {'mehmet', 'ahmet'})  # False
print({'name': 'ismail', 'surname': 'turut'} is {'surname': 'turut', 'name': 'ismail'})  # False


# Not. Degeri tersine cevirir
print('\not')
print(not 12 > 20)  # True
print(not True)  # False
print(not False)  # True
print('ali' not in ['ahmet', 'mehmet', 'isikara'])  # True
