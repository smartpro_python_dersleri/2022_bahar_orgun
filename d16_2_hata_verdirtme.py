def hata_verecek_fonksiyon(sayi):
    if sayi == 10:
        print('Şimdi hata vereceğiz')
        raise ValueError('HATA VAR ULAN!!!!')
    print(sayi)


hata_verecek_fonksiyon(2)
hata_verecek_fonksiyon(8)
hata_verecek_fonksiyon(4)
try:
    hata_verecek_fonksiyon(10)
except ValueError:
    print('Abi sen 10 girdin, ben 10 kabul etmiyorum!!!')
hata_verecek_fonksiyon(6)
