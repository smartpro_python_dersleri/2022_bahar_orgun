# Burada bir dosya açıp içerisine bir cümle yazdırıyoruz
with open('d14_yazilacak_dosya.txt', 'w') as file:
    file.write("Ben bir ceviz ağacıyım\n")
    print('Dosyamız başarıyla yazıldı!')
    print('-' * 5, '\n')


# Şimdi de yazdırdığımız dosyanın içeriğine bakalım
with open('d14_yazilacak_dosya.txt') as file:
    print(file.read())
    print('-' * 5, '\n')


# Burada da sonuna ekleme yapıyoruz
with open('d14_yazilacak_dosya.txt', 'a') as file:
    file.write('Gülhane parkında.\n')
    son_satirlar = [
        'Ne sen bunun farkındasın,\n',
        'Ne de polis farkında!\n'
    ]
    file.writelines(son_satirlar)
    print('Dosyamız başarıyla yazıldı!')
    print('-' * 5, '\n')


# Şimdi de yazdırdığımız dosyanın içeriğine bakalım
with open('d14_yazilacak_dosya.txt') as file:
    print(file.read())
