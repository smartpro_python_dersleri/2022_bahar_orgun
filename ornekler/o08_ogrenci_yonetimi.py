from typing import Union


def get_intput(
    text,
    is_num=False,
    is_task=False,
    tasklist=['add', 'list', 'del', 'select', 'quit'],
    rec=False
) -> Union[float, str]:
    if is_task and not rec:
        text += f' {str(tasklist)}'
    user_input = input(text + ': ')
    if is_num:
        try:
            return float(user_input)
        except ValueError:
            print('Please provide a number!')
            return get_intput(text, is_num, is_task, tasklist, rec=True)
    elif is_task and user_input.lower() in tasklist:
        return user_input.lower()
    elif is_task:
        print('Please provide one of the tasks spesified!\n' + str(tasklist))
        return get_intput(text, is_num, is_task, tasklist, rec=True)
    else:
        return user_input


class Student:
    __slots__ = ('name', 'surname', 'age', 'grades')

    def __init__(self, name, surname, age) -> None:
        self.name = name
        self.surname = surname
        self.age = age
        self.grades = []

    def __str__(self) -> str:
        return self.get_full_name()

    def get_full_name(self) -> str:
        return f'{self.name} {self.surname}'

    def print_student(self, index=None) -> None:
        if index is not None:
            print(f'{index} - {self} - GPA: {self.get_gpa()}')
        else:
            print(f'{self} - GPA: {self.get_gpa()}')

    def add_grade(self, grade: Union[int, float]) -> None:
        if type(grade) not in [int, float]:
            raise TypeError('Grade should be a "int" or "float"')
        self.grades.append(grade)

    def get_gpa(self) -> float:
        if self.grades:
            total = sum(self.grades)
            gpa = total / len(self.grades)
            return round(gpa, 2)
        return 0

    def is_passing(self) -> bool:
        if self.get_gpa() >= 45:
            return True
        return False


class StudentManager:
    __slots__ = ('students')

    def __init__(self) -> None:
        self.students = []

    def list_students(self) -> None:
        for index, student in enumerate(self.students):
            student.print_student(index=index)

    def create_new_student(self) -> None:
        name = get_intput('Name')
        surname = get_intput('Surname')
        age = get_intput('Age', is_num=True)

        self.students.append(
            Student(name, surname, age)
        )

    def search_student(self) -> Union[Student, None]:
        results = []
        search_word = get_intput('Search for user')

        for student in self.students:
            if search_word.lower() in student.get_full_name().lower():
                results.append(student)

        if not results:
            return None
        elif len(results) == 1:
            return results[0]
        else:
            print('Please select one of the choices:')
            for index, student in enumerate(results):
                student.print_student(index=index)
            while True:
                user_input = int(get_intput('Selected index', is_num=True))
                if 0 <= user_input < len(results):
                    return results[user_input]

    def delete_student(self) -> bool:
        student = self.search_student()

        if student in self.students:
            self.students.remove(student)
            return True
        else:
            print('Student does not exist!')
            return False

    def select_student(self) -> None:
        student = self.search_student()

        if not student:
            print('Student does not exist!')
            return False

        while True:
            student.print_student()
            task = get_intput('Please select a task!', is_task=True, tasklist=['add_grade', 'check_passing', 'quit'])
            match task:
                case 'add_grade':
                    grade = get_intput('Enter a new grade', is_num=True)
                    student.add_grade(grade)
                case 'check_passing':
                    if student.is_passing():
                        print('Student is passing with GPA:', student.get_gpa())
                    else:
                        print('Student is NOT passing with GPA:', student.get_gpa())
                case 'quit':
                    break


def main():
    manager = StudentManager()

    while True:
        task = get_intput('Please select a task', is_task=True)
        match task:
            case 'list':
                manager.list_students()
            case 'add':
                manager.create_new_student()
            case 'del':
                manager.delete_student()
            case 'select':
                manager.select_student()
            case 'quit':
                break
            case _:
                print(task)

    print('Good Night')


if __name__ == '__main__':
    main()
