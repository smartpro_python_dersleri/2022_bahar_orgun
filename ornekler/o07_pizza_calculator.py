from math import pi


def get_input(text):
    while True:
        inp = input(f'{text}: ')
        if inp.isdigit():
            return int(inp)
        print('Lütfen bir sayı giriniz!')


def create_pizza(pizza_size):
    diameter = get_input(f'{pizza_size} için çap')
    price = get_input(f'{pizza_size} için fiyat')
    return {'diameter': diameter, 'price': price}


def get_score(pizza):
    r = pizza['diameter'] / 2
    area = pi * r * r
    return area / pizza['price']


def main():
    sm_pizza = create_pizza('Küçük Pizza')
    lg_pizza = create_pizza('Büyük Pizza')

    sm_score = get_score(sm_pizza)
    lg_score = get_score(lg_pizza)

    print(f'Küçük Pizza Skoru: {sm_score}')
    print(f'Büyük Pizza Skoru: {lg_score}')

    if sm_score > lg_score:
        print('Küçük Pizza daha avantajlıdır!')
    elif lg_score > sm_score:
        print('Büyük Pizza daha avantajlıdır!')
    else:
        print('İlginçtir ki iki pizza da aynı verimliliktedir!')


if __name__ == '__main__':
    main()
