- Elimizde bir envanter listesi olacak
- Bu liste içersindeki elemanların özellikleri:
    * Eşya adı
    * Reyon numarası
    * Stok sayısı
- Ön tanımlı olarak 3 tane eşya olacak.
- Kullanıcı isterse yeni eşya ekleyebilecek
- Eşyanın stok sayısı değiştirilebilecek
- Kullanıcı eşya adından eşya arayabilecek ve reyonunu dönecek
- Bütün eşyalar listelenebilecek
- Stoklar ekleme çıkartma yöntemiyle de değiştirilebilir
- Her "stok değişimi", "çıkış", "ürün ekleme" işlemlerinde bir dosyaya "zaman" verisi ile de bir log yapılmalı
