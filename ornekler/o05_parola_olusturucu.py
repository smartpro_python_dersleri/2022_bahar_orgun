"""
- Bir parola oluşturma fonksiyonu yapılacak.
- Burada argüman olarak uzunluk, harf içerip içermediğiö büyük harf, özel karakter içerip içermediği.
- default olarak paroal 16 karakter olacak, sayı ve harf içerecek.

- Bu fonksiyon kullanıcıcan input alacak bir terminal uygulamasına import edilerek kullanılaca
"""

import string
from random import randint


def better_sample(sample_string, lenght):
    output = ''
    sample_lenght = len(sample_string)
    while len(output) < lenght:
        random_char = sample_string[randint(0, sample_lenght - 1)]
        output += random_char

    return output


def random_pass_generator(lenght=16, lower=True, upper=False, special=False):
    pass_sample = string.digits

    if lower:
        pass_sample += string.ascii_lowercase
    if upper:
        pass_sample += string.ascii_uppercase
    if special:
        pass_sample += string.punctuation

    password = better_sample(pass_sample, lenght)
    return password
