* Kesintisiz bir loop ile kullanıcıdan üç farklı input alınacak.
* Bu inputlardan iki tanesi sayı olacak.
* Üçüncü input ise operatör olacak.
* Hesap makinemizin destekleyeceği operatörler: `*, /, %, +, -`
* Sayı inputlarında sayı dışında bir şey alındığında hata verip en başa dönülecek
* Operatör inputlarında da belirtilen operatörler dışında bir şey gelirse kullanıcıya hata mesajı verilip en başa dönülecek.
* En sonda verilen operatör ve sayıların sırasını takip edilerek işlem yapılıp işlemin sonucu ekrana yazdırılacak.
* İşlem sonrasında program döngünün başına dönecek.
* Eğer kullanıcı herhangi bir input esnasında `q` derse, döngüden çıkılıp bir kapanma mesajı verilecek ve program kapatılacak.
