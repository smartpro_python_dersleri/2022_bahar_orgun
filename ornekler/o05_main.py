from o05_parola_olusturucu import random_pass_generator


def get_input(text):
    inp = input(f'{text} [y/N]: ')
    if inp.lower() == 'y':
        return True
    return False


def get_lenght():
    inp = input('Lütfen bir sayı giriniz. Default=16: ')
    if inp == '':
        return 16
    elif inp.isdigit():
        return int(inp)
    else:
        print('Girdiğiniz değer bir sayı değildir')
        return get_lenght()


def main():
    lenght = get_lenght()
    lower = get_input("Küçük harf içersin mi?")
    upper = get_input("Büyük harf içersin mi?")
    special = get_input("Özel karakter içersin mi?")

    password = random_pass_generator(
        lenght=lenght,
        lower=lower,
        upper=upper,
        special=special
    )
    print('Yeni parolanız:\n' + password)


if __name__ == '__main__':
    main()
