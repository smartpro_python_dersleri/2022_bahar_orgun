"""
* Bu ödevde sizden kendisine verilen cümle içerisinde spesifik bir karakter grubunu arayan bir fonsiyon yazmanızı istiyorum.
* Bu fonksiyonumuz iki farklı argümana sahip olacaktır. Bunlar: text, filter
* text argümanı içerisinde fonksiyona vereceğimiz filtrelenecek metin girilecek.
* filter argümanı ise hangi karakterleri sayacağımızı belirleyecek.
* filter'a verinin nasıl girileceği size kalmış.
* Örnek olarak bir cümle içerisinde bütün sesli harfleri sayabiliriz.
* Sayma işleminin sonunda bütün sonuçların bize bir dict olarak dönmesi lazım.
* Dict kalıbı `{"a": 20, "b": 5}` şeklinde olacaktır.
* Ardından başka bir fonksiyon ile de bu sonuçları ekrana aşağıdaki formatta yazdıracağız:
```
{string}

a = 20
b = 5
```
"""


def the_counter(text, filter):
    output = {}
    text = text.lower()
    for i in filter:
        output.update({
            i: text.count(i),
        })
    return output


def print_counts(count_list):
    for key, value in count_list.items():
        print(f'{key} = {value}')


text = "Sen Abdülhamidi savundun, hayır savunmadım, savundun, çıkart göster!"
filter = "aeıioöuü"

count_list = the_counter(text, filter)
print_counts(count_list)
