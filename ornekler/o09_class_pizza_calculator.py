from math import pi


def get_intput(text: str) -> float:
    try:
        user_input = input(text + ': ')
        return float(user_input)
    except ValueError:
        print('Please provide a number!')
        return get_intput(text)


class Pizza:
    __slots__ = ('size', 'diameter', 'price', '_area', '_score')

    def __init__(self, size: str, diameter: float, price: float) -> None:
        self.size = size
        self.diameter = diameter
        self.price = price
        self._area = None
        self._score = None

    def __str__(self) -> str:
        return f'Pizza with {self.diameter}cm diameter and {self.price}₺ price.'

    def __lt__(self, other: 'Pizza') -> bool:
        return self.score < other.score

    def __gt__(self, other: 'Pizza') -> bool:
        return self.score > other.score

    @classmethod
    def new(cls, size: str) -> 'Pizza':
        diameter = get_intput(f'Diameter for {size} pizza')
        price = get_intput(f'Price for {size} pizza')
        return cls(size, diameter, price)

    @property
    def area(self) -> float:
        if not self._area:
            r = self.diameter / 2
            r_square = r ** 2
            self._area = r_square * pi
        return self._area

    @property
    def score(self) -> float:
        if not self._score:
            self._score = self.area / self.price
        return self._score

    def pretty_score(self):
        return f'{self.size} Pizza: {round(self.score, 2)}cm\u00b2 per ₺'


def main():
    small_pizza = Pizza.new('Small')
    large_pizza = Pizza.new('Large')

    print(
        '\nSCORES:\n\t{}\n\t{}\n'.format(
            small_pizza.pretty_score(),
            large_pizza.pretty_score()
        )
    )

    if small_pizza > large_pizza:
        print('Small Pizza is more effective')
    elif small_pizza < large_pizza:
        print('Large Pizza is more effective')
    else:
        print('Your pizza maker is an OCD math professor in disguise!')


if __name__ == "__main__":
    main()
