* 1'den 50'ye kadar sayılacak. 50'yi de kapsayacak.
* Eğer sayı 3'e bölünüyorsa ekrana "Fizz" yazılacak.
* Eğer sayı 5'e bölünüyorsa ekrana "Buzz" yazılacak.
* Eğer sayı hem 3'e hem de 5'e bölünüyorsa ekrana "FizzBuzz" yazılacak.
* Sayı ne 3'e ne de 5'e bölünüyorsa direkt olarak sayı yazılacak.
