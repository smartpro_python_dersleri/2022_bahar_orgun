student_list = []

task_list = ['list', 'add', 'select', 'quit']


def get_input(text, is_task=False, is_int=False, t_list=task_list):
    if is_task:
        text = f'{text}: {t_list} '
    else:
        text = f'{text}: '
    while True:
        inp = input(text)

        if is_task and inp.lower() == 'quit':
            print('Programdan başarılı bir biçimde çıktınız.')
            exit()
        elif is_task and inp.lower() in t_list:
            return inp.lower()
        elif is_task and inp.lower() not in t_list:
            print(f'"{inp}" hatalı bir işlemdir!')
            continue
        elif is_int and inp.isdigit():
            return int(inp)
        elif is_int and not inp.isdigit():
            print('Lütfen bir sayı giriniz!')
            continue
        else:
            return inp


def add_student():
    print('Girmek istediğiniz öğrencinin bilgilerini yazınız:')
    name = get_input('Adı')
    surname = get_input('Soyadı')
    age = get_input('Yaş')
    croom = get_input('Sınıf')

    student = {
        'name': name,
        'surname': surname,
        'age': age,
        'classroom': croom,
    }
    student_list.append(student)
    print('Öğrenci başarılı bir biçimde sisteme kaydedilmiştir.')


def format_student(student):
    return f'{student["name"]} {student["surname"]} - {student["age"]} - {student["classroom"]}'


def list_students():
    for index, student in enumerate(student_list):
        print(f'{index}: {format_student(student)}')


def select_student():
    while True:
        index = get_input('Lütfen geçerli bir öğrenci indexi giriniz', is_int=True)
        if index >= len(student_list):
            print(f'{index} geçerli bir index değildir.')
            continue
        student = student_list[index]
        print(format_student(student))
        break


def main():
    while True:
        task = get_input('Lütfen geçerli bir işlem seçiniz', is_task=True)

        if task == 'add':
            add_student()
        elif task == 'list':
            list_students()
        elif task == 'select':
            select_student()


if __name__ == "__main__":
    main()
