* Bu ödevde sizden kendisine verilen cümle içerisinde spesifik bir karakter grubunu arayan bir fonsiyon yazmanızı istiyorum.
* Bu fonksiyonumuz iki farklı argümana sahip olacaktır. Bunlar: text, filter
* text argümanı içerisinde fonksiyona vereceğimiz filtrelenecek metin girilecek.
* filter argümanı ise hangi karakterleri sayacağımızı belirleyecek.
* filter'a verinin nasıl girileceği size kalmış.
* Örnek olarak bir cümle içerisinde bütün sesli harfleri sayabiliriz.
* Sayma işleminin sonunda bütün sonuçların bize bir dict olarak dönmesi lazım.
* Dict kalıbı `{"a": 20, "b": 5}` şeklinde olacaktır.
* Ardından başka bir fonksiyon ile de bu sonuçları ekrana aşağıdaki formatta yazdıracağız:
```
{string}

a = 20
b = 5
```
