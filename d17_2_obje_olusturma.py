class Student:
    def __init__(self, name, surname, age, subject):
        self.name = name
        self.surname = surname
        self.age = age
        self.subject = subject
        self.marks = []

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        return f'{self.name} {self.surname}'

    def add_new_mark(self, mark: int):
        if type(mark) != int:
            raise TypeError('"mark" should be an "int"! BRO!!!')
        elif mark > 100 or mark < 0:
            raise ValueError('"mark" should be between 0 and 100! BRO!!!')
        self.marks.append(mark)

    def add_multiple_marks(self, marks: list[int]):
        try:
            for i in marks:
                self.add_new_mark(i)
        except Exception as e:
            print(e)

    def calculate_gpa(self):
        if self.marks:
            total = sum(self.marks)
            return round(total / len(self.marks), 2)
        return 0

    def get_older(self):
        self.age += 1


mahmut = Student(
    name='Mahmut',
    surname='Tuncer',
    age='59',
    subject='Lo',
)

if __name__ == '__main__':
    print(mahmut)
    print(type(mahmut))

    print(mahmut.marks)
    mahmut.add_multiple_marks([12, 90, 5, 10, 20, 11])
    print(mahmut.marks)

    print(mahmut.calculate_gpa())
