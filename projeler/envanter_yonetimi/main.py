from typing import Union

from file_utils import read_from_file, write_to_file, create_log
from models import Item
from get_input import get_task_input, get_float_input, get_str_input


class ItemManager:
    __slots__ = ('items')

    def __init__(self) -> None:
        self.items = []
        raw_data = read_from_file()
        for i in raw_data:
            self.items.append(
                Item(i['name'], i['price'], i['stock'], i['shelf'])
            )

    def save_to_db(self) -> None:
        data = [item.to_json() for item in self.items]
        write_to_file(data)

    def list_items(self) -> None:
        for index, item in enumerate(self.items):
            print(f"{index} | {item.pretty_print()}")

    def get_item(self) -> Union[Item, None]:
        search_term = get_str_input('Search in item name')

        results = []
        for item in self.items:
            if search_term.lower() in item.name.lower():
                results.append(item)

        match len(results):
            case 0:
                return None
            case 1:
                return results[0]

        for index, item in enumerate(results):
            print(f"{index} | {item.pretty_print()}")
        while True:
            user_input = int(get_float_input("Select an item by index"))
            if user_input in range(0, len(results)):
                return results[user_input]
            print(f'Please provide a number between 0 and {len(results)}')

    def search_item(self) -> None:
        item = self.get_item()

        task_list = ['sell', 'restock', 'remove', 'quit']
        while True:
            print(item.pretty_print())
            task = get_task_input('Please select a task', task_list)
            match task.lower():
                case 'quit':
                    self.save_to_db()
                    print('Ok mate!')
                    break
                case 'sell':
                    number = int(get_float_input('Number of sales'))
                    item.sell(number)
                case 'restock':
                    number = int(get_float_input('Number of new stocks'))
                    item.restock(number)
                case 'remove':
                    name = item.name
                    self.items.remove(item)
                    create_log('DEL', 'Item', name)
                    self.save_to_db()
                    break
            self.save_to_db()

    def main(self) -> None:
        task_list = ['add', 'list', 'search', 'quit']

        while True:
            task = get_task_input('Please select a task', task_list)
            match task.lower():
                case 'quit':
                    self.save_to_db()
                    print('Goodbye world!')
                    break
                case 'add':
                    new_item = Item.new()
                    self.items.append(new_item)
                    self.save_to_db()
                case 'list':
                    self.list_items()
                case 'search':
                    self.search_item()


if __name__ == "__main__":
    manager = ItemManager()
    manager.main()
