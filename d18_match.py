def main():
    user_input = input('1 ile 3 arası bir sayı girin: ')

    match user_input:
        case '1':
            print('Kullanıcı "1" girdi!')
        case '2':
            print('Kullanıcı "2" girdi!')
        case '3':
            print('Kullanıcı "3" girdi!')
        case _:
            print('Kullanıcı kafasına göre bir şey girdi!')

    # Yukarıdaki Match aşağıdaki If ile birebir aynı işlemi yapar
    """
    if user_input == '1':
        print('Kullanıcı "1" girdi!')
    elif user_input == '2':
        print('Kullanıcı "2" girdi!')
    elif user_input == '3':
        print('Kullanıcı "3" girdi!')
    else:
        print('Kullanıcı kafasına göre bir şey girdi!')
    """


if __name__ == "__main__":
    main()
