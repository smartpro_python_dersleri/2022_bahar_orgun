"""
Python içerisinde Type denetleme şu an için mevcut değildir.
Şu an sadece Type hinting yani ipucu verme şeklinde çalışmaktadır.
Bu sadece kullandığınız editör, kodu okurken siz vb gibi durumlarda işe yarar.
Eğer kodunuza zorunlu bir tip doğrulama eklemek isterseniz bunu sizin kendinizin yapması lazım
"""

from typing import get_type_hints


def string_ve_int_fonksiyon(string: str, integer: int) -> str:
    if type(string) != str:
        raise TypeError('first args should be a "str"')
    if type(integer) != int:
        raise TypeError('second args should be an "int"')
    print(type(string))
    print(type(integer))
    return string * integer


print(get_type_hints(string_ve_int_fonksiyon))


print(string_ve_int_fonksiyon('Mahmut', 12))
print(string_ve_int_fonksiyon(20, 12))
