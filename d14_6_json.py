import json


ogrenci_listesi = [
    {
        'name': 'Mahmut',
        'surname': 'Tuncer',
        'age': '99',
        'classroom': 'Flash TV',
    },
    {
        'name': 'İbrahim',
        'surname': 'Tatlıses',
        'age': '82',
        'classroom': 'IboShow',
    },
    {
        'name': 'Müslüm',
        'surname': 'Gürses',
        'age': 'infinity',
        'classroom': 'our_hearts',
    },
]

with open('d14_json_ornek.json', 'w') as file:
    json_output = json.dumps(ogrenci_listesi)
    file.write(json_output)


with open('d14_json_ornek.json') as file:
    result = file.read()
    print(type(result))

    result_list = json.loads(result)
    print(type(result_list))

    for i in result_list:
        print(i)
