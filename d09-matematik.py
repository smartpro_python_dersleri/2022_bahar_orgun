a = 12 + 10
print(a * 3)
print(22 * 3)
print(22 == a)

print(20 / 3)
print(int(25 / 5))
print(25 / 5)

print(42 - 10)
print(10 * -1)
print(0 / 10)
# print(10 / 0)  # Hata verir

print(42 % 5)

if 55 % 3 == 0:
    print('55 3\'e bolunur')
elif 55 % 5 == 0:
    print('55 5\'e bolunur')

# print(55 % 0)
