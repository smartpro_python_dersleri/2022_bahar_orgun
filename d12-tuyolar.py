deneme = "print('Mahmut')"

exec(deneme)
eval(deneme)

sayi1 = 12
sayi2 = 20
op = '*'

print(f'{sayi1} {op} {sayi2} = {eval(f"{sayi1}{op}{sayi2}")}')

"""
super_secret_key = "Bu verinin dışarı çıkmaması lazım"

ornek_problemli_input = input('Burada problemli bir input almaya çalışacağız')

eval(ornek_problemli_input)
"""

ornek_str = "Ben bir ceviz ağacıyım Gülhane parkında.\nNe sen bunun farkındasın, ne de polis farkında."

print(ornek_str.lower())
print(ornek_str.upper())
print(ornek_str.capitalize())

print(ornek_str.split())
print(ornek_str.split(','))

print('n', ornek_str.lower().count('n'))

ornek_liste = ['ali', 'veli', 'mahmut', 'ali', 'huseyin', 'Ali']
print(''.join(ornek_liste))

print('ali', ornek_liste.count('ali'))
print('mahmut', ornek_liste.count('mahmut'))

for i in 'Merhabalar':
    print(i)
