ogrenci_listesi = []

with open('student_list.txt') as file:
    for i in file.readlines():
        ogrenci = i.split('%')
        ogrenci_listesi.append({
            'name': ogrenci[0],
            'surname': ogrenci[1],
            'age': ogrenci[2],
            'classroom': ogrenci[3].replace('\n', '')
        })


for i in ogrenci_listesi:
    print(i)
