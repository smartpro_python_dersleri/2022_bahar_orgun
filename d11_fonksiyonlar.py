def benim_ilk_fonksiyonum():
    print('Aşağıdakı kalıbı tamamlayınız')
    inp = input('Doğada bulunan dört element, Ateş, Su, Toprak... ')
    if inp.lower() in ['hava', 'tahta']:
        print(f'Tebrikler, "{inp}" doğru bir yanıttır.')
    else:
        print('YANLIŞ')


# benim_ilk_fonksiyonum()

mahmut = benim_ilk_fonksiyonum
# mahmut()


def get_input(text):
    inp = input(text)
    if inp.lower() == 'q':
        exit()
    elif inp.isdigit():
        return int(inp)
    else:
        return 'Aga bu olmadi'


# sonuc = get_input('Bana bir sayi ver abi: ')
# print(sonuc)
# print(type(sonuc))

# Burasi arguman vermedigimiz icin hata verir
# sonuc2 = get_input()


def arguman_zengini_fonksiyon(pos1, pos2, pos3, key1=None, key2='Mahmut', key3=42):
    print(f'pos1 = {pos1}')
    print(f'pos2 = {pos2}')
    print(f'pos3 = {pos3}')
    print(f'key1 = {key1}')
    print(f'key2 = {key2}')
    print(f'key3 = {key3}')


# arguman_zengini_fonksiyon('ali', 'veli', 'hasan', key1='Mustafa', key3='Ibraim')
# arguman_zengini_fonksiyon(pos2='ali', pos3='veli', pos1='hasan', key1='Mustafa', key3='Ibraim')
#
# arguman_zengini_fonksiyon(11, 12, 13, 14)


def gelismis_get_input(text, is_int=False):
    while True:
        inp = input(text)
        if inp.lower() == 'q':
            print('Gorusuruz gencler')
            exit()
        elif is_int and inp.isdigit():
            return int(inp)
        elif is_int and not inp.isdigit():
            print('AGA BU BIR SAYI DEGIL')
        else:
            return inp


# deneme = gelismis_get_input('Bana bir sayi ver: ', is_int=True)
# print(deneme, type(deneme))
#
# deneme2 = gelismis_get_input('Simdi de operator ver abi: ')
# print(deneme2, type(deneme2))


def coklu_returnlu_fonksiyon():
    return 'Mahmut', 'Tuncer'


coklu_degisken = coklu_returnlu_fonksiyon()
print(type(coklu_degisken))
print(coklu_degisken)
name, surname = coklu_returnlu_fonksiyon()

print(name, type(name))
print(surname, type(surname))
