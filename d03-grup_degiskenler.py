# tuple:

ornek_tuple_1 = 'Ali', 'Veli', 'Mahmut'
ornek_tuple_2 = ('Hasan', 'Huseyin', 'Ibrahim')

# print(type(ornek_tuple_1))
# print(ornek_tuple_2)

# print(ornek_tuple_1[0], ornek_tuple_2[-1])

print(ornek_tuple_1[::-1])


# List

ornek_list = ['Suheyb', 'Cemre', 'Kemal', 'Deniz']
# print(ornek_list)

ornek_list.append('Goktug')
# print(ornek_list)

cikarilan = ornek_list.pop(-1)
# print(ornek_list)
# print(cikarilan)

cikarilan_remove = ornek_list.remove('Suheyb')
# print(ornek_list)
# print(cikarilan_remove)

# Set

ornek_list_set = set(ornek_list)
ornek_list_set_list = list(ornek_list_set)

ornek_set = {'Kedi', 'Kopek', 'Kokarca', 'Kopek Baligi'}
# print(ornek_set)


# Dict

ornek_dict = {'name': 'Mahmut', 'surname': 'Tuncer'}

# print(ornek_dict)
# print(ornek_dict['name'])

ornek_dict['name'] = 'Mustafa'
ornek_dict['surname'] = 'Sandal'
# print(ornek_dict)

ornek_dict.update({'age': 52})
ornek_dict['age'] += 1
# print(ornek_dict)

ornek_dict.get('name')

print(ornek_dict.get('gender'))
# print(ornek_dict['gender'])

yeni_ornek_dict = {
    'name': 'Yildiz',
    'surname': 'Tilbe',
    'name': 'Ismail'
}
# print(yeni_ornek_dict)

print(ornek_dict.keys())
print(ornek_dict.values())
print(ornek_dict.items())
