class Kedi:
    name = None
    color = None
    gender = None
    breed = None
    age = None

    def __str__(self):
        return self.name

    def get_info(self):
        pro = ["He", "Him"] if self.gender == 'Male' else ['She', 'Her']
        if self.age >= 8:
            print(f'{pro[0]} is an old cat, take a good care of {pro[1]}')
        elif self.age <= 1:
            print(f'{pro[0]} is just a baby, take a good care of {pro[1]}')

        print('{} is a {} years old {}, {} self'.format(
            self.name, self.age, self.gender, self.breed
        ))
        print(f'And {pro[0]} is a good cat!')

    def get_older(self):
        self.age += 1


pacchi = Kedi()

pacchi.name = 'Pacchi'
pacchi.color = 'Tri-color'
pacchi.breed = 'Tekir / Mozaik Kek Kedisi'
pacchi.gender = 'Female'
pacchi.age = 2

pamuk = Kedi()
pamuk.name = 'Pamuk'
pamuk.color = 'White/Yellow'
pamuk.breed = 'Tekir'
pamuk.gender = 'Male'
pamuk.age = 1

pacchi.get_info()
print('-' * 8)

print(pacchi)

pacchi.get_older()
pacchi.get_info()
print(pacchi.age)


"""
def get_cat_info(cat: Kedi):
    pro = ["He", "Him", 'His'] if cat.gender == 'Male' else ['She', 'Her', 'Her']
    if cat.age >= 8:
        print(f'{pro[0]} is an old cat, take a good care of {pro[1]}')
    elif cat.age <= 1:
        print(f'{pro[0]} is just a baby, take a good care of {pro[1]}')

    print('{} is a {} years old {}, {} cat'.format(
        cat.name, cat.age, cat.gender, cat.breed
    ))
    print(f'And {pro[0]} is a good cat!')


get_cat_info(pacchi)
print('-' * 8)
get_cat_info(pamuk)
"""
