class Insan:
    def __init__(self, name: str, surname: str, age: int) -> None:
        self.name = name
        self.surname = surname
        self.age = age

    def __str__(self) -> str:
        return self.get_fullname()

    def get_fullname(self) -> str:
        return f'{self.name} {self.surname}'


class TitleMixin:
    def add_title(self, title: str) -> None:
        self.title = title

    def get_fullname(self) -> str:
        try:
            fullname = super().get_fullname()
            if self.title:
                return f'{self.title} {fullname}'
            else:
                return fullname
        except Exception as e:
            print(e, e.args, e.__class__)
            exit()


class Doctor(TitleMixin, Insan):
    def __init__(self, name: str, surname: str, age: int, title: str) -> None:
        super().__init__(name, surname, age)
        self.title = title


class Engineer(TitleMixin, Insan):
    def __init__(self, name: str, surname: str, age: int, profession: str) -> None:
        super().__init__(name, surname, age)
        self.title = None
        self.profession = profession


class Hayvan:
    def __init__(self, name: str, age: int, gender: str) -> None:
        self.name = name
        self.age = age
        self.gender = gender


# Hayvan classında `get_fullname` methodu olmadığı için `TitleMixin` altındaki `get_fullname` hata verecektir
class SuperCat(TitleMixin, Hayvan):
    pass


def main() -> None:
    doc10 = Doctor('David', 'Tennant', 903, 'Dr.')

    print(doc10.get_fullname())

    kemal = Engineer('Kemal', 'Kocabaş', 29, 'Programmer')

    print(kemal.get_fullname())
    kemal.add_title('Sr. Dev.')
    print(kemal.get_fullname())

    pacchi = SuperCat('Pacchi', 2, 'Female')
    print(pacchi.name)
    print(pacchi.get_fullname())


if __name__ == "__main__":
    main()
