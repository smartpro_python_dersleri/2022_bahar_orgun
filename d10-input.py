# Kullanicidan bir deger almak istiyorsak burada input() komutunu kullaniyoruz

while True:
    a = input('Bana bir deger ver abi, cikmak istersen de "q" yaz: ')
    if a == 'q':
        break
    print('Type:', type(a))
    print('Is Digit:', a.isdigit())
    # int(a)  # Burasi eger sayi olmayan bir sey alirsa hata donecektir

    if a.isdigit():
        int_a = int(a)
        print(int_a)
        print(type(int_a))
    else:
        print('Abi bu sayi mayi degil')

print('Hoccakalin ben gidiyom')
