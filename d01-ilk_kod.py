ornek = "Merhaba Sınıf"
print(ornek)

# bu bir yorum satiridir
ornek = "Merhaba Dunya"
print(ornek)  # bu da bir yorumdur


def ornek_fonksiyon():
    print(ornek, ornek)
    print('Ornek fonsiyonun ikinci satiri')


print('Burasi ornek fonsiyonun disinda')
ornek_fonksiyon()

"""
Bu bir yorum parantezidir.
Burada yazacigimiz hicbir sey kodda calismayacaktir
"""
