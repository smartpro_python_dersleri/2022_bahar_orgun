"""
Python bazı spesifik işlemleri objelerimide yapabilmek için belli methodları bekler
Bu methodlar o işlemin bizim objemiz ile nasıl yapılacağını belirtir.
Şu ana kadar onlardan bir tanesi ile tanıştık.
"""
from math import pi


class Pizza:
    def __init__(self, diameter: float, price: float, ingrediants: list[str]):
        self.diameter = diameter
        self.price = price
        self.ingrediants = ingrediants

    # Bu method objemizin string haline geldiğinde ne olacağını belirler
    def __str__(self):
        str_ings = ', '.join(self.ingrediants)
        return f'Bir adet {str_ings} malzemeli {self.get_size_type().lower()} pizza'

    def get_size_type(self):
        if self.diameter < 22:
            return 'Küçük'
        elif self.diameter < 30:
            return 'Orta'
        else:
            return 'Büyük'

    def get_area(self):
        radius = self.diameter / 2
        return radius * radius * pi

    # __add__ methodu bizim objelerimize toplama özelliği eklememizi sağlıyor
    def __add__(self, other):
        if type(other) == self.__class__:
            return self.get_area() + other.get_area()
        else:
            raise TypeError('Please use a Pizza object')

    # __sub__ methodu da bizim objelerimize çıkartma özelliği eklememizi sağlıyor
    def __sub__(self, other):
        if type(other) == self.__class__:
            return self.get_area() - other.get_area()
        else:
            raise TypeError('Please use a Pizza object')


pizza1 = Pizza(
    diameter=26,
    price=60,
    ingrediants=['Mozerella', 'Fesleğen', 'Domates']
)

pizza2 = Pizza(
    diameter=32,
    price=95,
    ingrediants=['Ananas', 'Füme Et', 'Fesleğen']
)

print(pizza1)
print(pizza2)

print('-' * 20)

print(f'İki pizzamızın alanlarının toplamı: {pizza1 + pizza2}')

# print(pizza1 + 30)
print(pizza1.get_area())
print(pizza2.get_area())
print(f'İki pizzanın alan farkı: {pizza1 - pizza2}')


pizza1.name = "Margarita"
print(pizza1.name)


# Slotlar bize objemizin sadece bu iceriklere sahip olacagina dair bir kisitlama sagliyor
class PizzaWithSlots:
    __slots__ = ('diameter', 'price', 'ingrediants')

    def __init__(self, diameter: float, price: float, ingrediants: list):
        self.diameter = diameter
        self.price = price
        self.ingrediants = ingrediants

    # Bu method objemizin string haline geldiğinde ne olacağını belirler
    def __str__(self):
        str_ings = ', '.join(self.ingrediants)
        return f'Bir adet {str_ings} malzemeli {self.get_size_type().lower()} pizza'

    def get_size_type(self):
        if self.diameter < 22:
            return 'Küçük'
        elif self.diameter < 30:
            return 'Orta'
        else:
            return 'Büyük'


pizza3 = PizzaWithSlots(
    diameter=18,
    price=30,
    ingrediants=['Sucuk', 'Kaşar', 'Pastırma']
)

print(pizza3)

# Burasi bize AttributeError verecek
# pizza3.name = 'Acıtasyon'
# print(pizza3.name)
